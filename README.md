# linux-postmarketos-archive

Archive of the issues in linux-postmarketos.git

* The linux-postmarketos.git repository was removed due to space issues ([postmarketOS#47](https://gitlab.com/postmarketOS/postmarketos/-/issues/47))
* This gitlab project holds the issues from that repo for future reference
* Note that this kernel is/was not used for all (close to mainline) devices in postmarketOS, only for some. Check the depends of a device's package to figure out which kernel it uses ([example](https://gitlab.com/postmarketOS/pmaports/-/blob/b693cdaac781abc5eadec78126c55dfb43d18174/device/main/device-pine64-pinephone/APKBUILD#L31)). Check the APKBUILD of that kernel package to find out where the source is hosted, who maintains it etc.
